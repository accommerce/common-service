const { Shops, Users } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

exports.getShopById = async (id) => {
    const shop = await Shops.findById(id)
        .populate({
            path: 'seller',
            model: Users,
        })
        .lean()

    if (!shop) {
        throw CustomError(`Cửa hàng không tồn tại`, `Shop ${id} does not exist!`, 404)
    }

    return shop
}
