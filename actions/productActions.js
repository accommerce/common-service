const Bluebird = require('bluebird')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { Products, Sizes, Shops, Users, Comments } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

const _searchProductsAndAttachSizes = async (query, page, limit, sort) => {
    const products = await Products.find(query)
        .populate({
            path: 'shop',
            model: Shops,
            populate: {
                path: 'seller',
                model: Users,
            },
        })
        .skip((page - 1) * limit)
        .limit(limit)
        .sort(sort)
        .lean()

    return Bluebird.map(
        products,
        async (product) => {
            const sizes = await Sizes.find({ product: product._id })
                .select('-_id name numberInStock')
                .lean()

            return { ...product, sizes }
        },
        { concurrency: products.length }
    )
}

exports.searchProducts = async (
    search = '',
    shopId = null,
    page = 1,
    limit = 20,
    sort = '-_id'
) => {
    const vPage = parseInt(page)
    const vLimit = parseInt(limit)

    const query = { deletedAt: null }

    if (shopId) {
        query.shop = shopId
    }

    if (search) {
        query.$text = { $search: search }
    }

    const [products, total] = await Bluebird.all([
        _searchProductsAndAttachSizes(query, vPage, vLimit, sort),
        Products.countDocuments(query),
    ])

    const pages = Math.ceil(total / vLimit)

    return { products, total, pages, page: vPage }
}

exports.getProductById = async (id, sellerId) => {
    const product = await Products.findOne({ _id: id, deletedAt: null })
        .populate({
            path: 'shop',
            model: Shops,
            populate: {
                path: 'seller',
                model: Users,
            },
        })
        .lean()

    if (!product) {
        throw CustomError(`Sản phẩm không tồn tại`, `Product ${id} not found!`, 404)
    }

    if (!sellerId || sellerId.toString() !== product?.shop?.seller?._id?.toString()) {
        Products.updateOne({ _id: id }, { $inc: { views: 1 } }).exec()
    }

    const sizes = await Sizes.find({ product: product._id })
        .select('-_id name numberInStock')
        .lean()

    return { ...product, sizes }
}

exports.getProductComments = async (productId) => {
    const { error } = Joi.objectId().validate(productId)
    if (error) {
        throw error
    }

    return Comments.find({ product: productId })
        .populate({
            path: 'author',
            model: Users,
        })
        .sort({ _id: -1 })
        .lean()
}
