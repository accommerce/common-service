const express = require('express')
const router = express.Router({ mergeParams: true })
const productController = require('../controllers/productController.js')

router.get('/', productController.searchProducts)
router.get('/:productId', productController.getProductById)
router.get('/:productId/comments', productController.getProductComments)


module.exports = router
