const express = require('express')
const router = express.Router()
const shopController = require('../controllers/shopController.js')
const productsRouter = require('../routes/products')

router.get('/:shopId', shopController.getShopById)

router.use('/:shopId/products', productsRouter)

module.exports = router
