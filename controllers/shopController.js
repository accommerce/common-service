const sendResponse = require('../helpers/sendResponse.js')
const { getShopById } = require('../actions/shopActions.js')

exports.getShopById = async (req, res, next) => {
    try {
        const { shopId } = req.params

        const shop = await getShopById(shopId)

        sendResponse(shop)(req, res, next)
    } catch (error) {
        next(error)
    }
}
