const sendResponse = require('../helpers/sendResponse.js')
const {
    getProductById,
    searchProducts,
    getProductComments,
} = require('../actions/productActions.js')

exports.searchProducts = async (req, res, next) => {
    try {
        const { shopId, search, page, limit, sort } = { ...req.params, ...req.query }

        const products = await searchProducts(search, shopId, page, limit, sort)

        sendResponse(products)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getProductById = async (req, res, next) => {
    try {
        const { productId } = req.params
        const { _id: sellerId } = req.user || {}

        const product = await getProductById(productId, sellerId)

        sendResponse(product)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getProductComments = async (req, res, next) => {
    try {
        const { productId } = req.params

        const comments = await getProductComments(productId)

        sendResponse(comments)(req, res, next)
    } catch (error) {
        next(error)
    }
}
